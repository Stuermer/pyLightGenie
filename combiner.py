import pathlib
from itertools import combinations

import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate
import scipy.optimize
from joblib import Parallel, delayed
from tqdm import tqdm
from typing import List


class Source:
    def __init__(self, wavelength, intensity, name):
        self.wavelength = wavelength
        self.intensity = intensity
        self.min_wl = np.min(wavelength)
        self.max_wl = np.max(wavelength)
        self.name = name

        self.ip = scipy.interpolate.interp1d(self.wavelength, self.intensity)

    def get_flux(self, wavelength):
        return self.ip(wavelength)

    def __str__(self):
        return str(self.name)


def select_sources(wavelength, min_flux=10.) -> List[Source]:
    """
    Preselects sources based on wavelength range and minimal flux within that range

    Args:
        wavelength: wavelength range of interest
        min_flux: minimum flux required to be within target wavelength range

    Returns:
        list of Sources

    """
    selection = []
    sources = list(pathlib.Path('data/sources').glob('**/*.csv'))  # all sources

    for source in sources:
            led = Source(* np.genfromtxt(source.resolve(), delimiter=',', skip_header=1).T, source.resolve())
            flux = led.get_flux(wavelength)
            flux[flux < min_flux/100. * np.max(led.intensity)] = 0.
            if np.sum(flux) > min_flux:
                selection.append(led)
                print(source, np.sum(flux))

    return selection


def optimize_fluxes(sources: tuple, target_wl, target_flux):
    """
    Optimizes flux levels for given collection of sources to match target flux

    Args:
        sources: Source objects to be combined
        target_wl: wavelength range of interest
        target_flux: target flux of combined spectrum

    Returns:
        optimization result, p1.x contains optimal weights of source objects

    """
    p0 = np.array([1.] * len(sources))  # initial parameters
    fluxes = np.array([s.get_flux(target_wl) for s in sources])  # fluxes in target wavelength range

    def errorfunc(p):
        return target_flux - np.dot(p, fluxes)

    p1 = scipy.optimize.least_squares(errorfunc, p0, bounds=np.array([(0, np.inf)] * len(sources)).T)
    return p1


def plot_combination(combo, weights, target_wl, target_flux):
    """
    Plots combination of sources and how well it matches the target flux

    Args:
        combo: collection of light sources
        weights: optimized relative weights of light sources
        target_wl: target wavelength range
        target_flux: target flux

    Returns:
        None
    """
    plt.figure()
    plt.title('Combined spectrum')
    plt.xlabel('Wavelength [nm]')
    plt.ylabel('Flux')
    plt.plot(target_wl, target_flux, linewidth=3, label='target')
    total_flux = np.zeros_like(target_wl, dtype=float)
    for w, lamp in zip(weights, combo):
        plt.plot(target_wl, w * lamp.get_flux(target_wl), label=f'{lamp.name.stem}',
                 linestyle='dashed')
        total_flux += w * lamp.get_flux(target_wl)

    plt.plot(target_wl, total_flux, linewidth=3, label="sum")
    plt.legend()
    plt.show()


def find_best_combination(target_wl, target_spectrum, number_of_sources=3):
    """
    Finds the best combination of N sources that matches the target spectrum.

    Args:
        target_wl: target wavelength range
        target_spectrum: target flux
        number_of_sources: number of sources to combine

    Returns:
        tuple of source combination and optimized relative weights
    """
    selected_sources = select_sources(target_wl)  # selecting all sources that have significant flux in wavelength range
    combinations_list = list(combinations(selected_sources, number_of_sources))  # all possible combination of sources

    print(f'There are {len(combinations_list)} combinations...')

    # optimize flux for each combination
    results = Parallel(n_jobs=4)(delayed(optimize_fluxes)(combinations_list[i], target_wl, target_spectrum) for i in
                                 tqdm(range(len(combinations_list))))

    # select best combo
    best = 0
    for i, result in enumerate(results):
        if result.cost < results[best].cost:
            best = i

    return combinations_list[best], results[best].x


if __name__ == "__main__":
    target_wl = np.arange(400, 800, dtype=float)
    target_flux = np.array([100.] * len(target_wl)) * np.linspace(1, 0.4, len(target_wl))
    combo, weights = find_best_combination(target_wl, target_flux, number_of_sources=3)
    plot_combination(combo, weights, target_wl, target_flux)
