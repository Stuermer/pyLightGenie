# pyLightGenie

The purpose of this package is to find the optimal combination of source spectra to match a certain target spectrum.

Based on a database of light sources (TODO: and filters) and an arbitrary target spectrum,  the script looks for the
best matching linear combination of the light sources. 
The user can select how many light sources can be used for combination.

> **WARNING**: Although the code does some preselection of light sources based on the target spectrum, the number of combinations 
> will grow rapidly when combining many light sources and/or using broad target wavelength ranges.

## Example

The example shown below here calculates the best combination of 3 light sources to match the target spectrum:

```python
if __name__ == "__main__":
    target_wl = np.arange(400, 800, dtype=float)
    target_flux = np.array([100.] * len(target_wl)) * np.linspace(1, 0.4, len(target_wl))
    combo, weights = find_best_combination(target_wl, target_flux, number_of_sources=3)
    plot_combination(combo, weights, target_wl, target_flux)
 ```

gives

![combined spectrum](docs/source/_static/example.png "Combined Spectrum")

## Source database

Currently, the database contains LEDs from Thorlabs (https://www.thorlabs.com/newgrouppage9.cfm?objectgroup_ID=6071).
More lamps are about to be added.

You can simply add more source by saving **.csv** files in *data/sources* with two columns one for wavelength [nm] and 
one for normalized flux.

## Filters
TODO: implement 