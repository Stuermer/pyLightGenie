# this script is to convert thorlabs LED specs to .csv files
# https://www.thorlabs.com/newgrouppage9.cfm?objectgroup_ID=6071

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.signal import savgol_filter

df = pd.read_excel('MCPCB_LED_Spectra_MxxxDx-Scaled_E62.xlsx', 1, header=[0, 1], skiprows=[1,2],
                   dtype={'Spectral Flux (mW/nm)': float})
df.dropna(axis=1, how='all', inplace=True)

lamps = {}
for lamp_name in df.columns.get_level_values(0).unique():
    if 'Wavelength (nm)' in df[lamp_name].columns:
        lamps[lamp_name] = df[lamp_name].set_index('Wavelength (nm)')
    else:
        print(f'{lamp_name} has no Wavelength column')

wavelength_grid = np.linspace(200, 2500, 10000, dtype=float)
df_wl = pd.DataFrame(wavelength_grid, columns=['Wavelength (nm)']).set_index("Wavelength (nm)")

plt.figure()
for ln, lamp in lamps.items():
    print(ln)
    lamp = lamp[lamp.index.notnull()]
    lamp = lamp[lamp.index.values > 0]
    lamp = lamp[~lamp.index.duplicated(keep='first')]
    lamp.sort_index(inplace=True)
    lamp = lamp.filter(['Spectral Flux (mW/nm)'])
    plt.plot(lamp['Spectral Flux (mW/nm)'], label=ln)
    lamp['Spectral Flux (mW/nm)'] = savgol_filter(lamp['Spectral Flux (mW/nm)'], 51, 3)
    # normalize it
    lamp['Spectral Flux (mW/nm)'] = lamp['Spectral Flux (mW/nm)'] / np.max(lamp['Spectral Flux (mW/nm)'])
    # w
    plt.plot(lamp['Spectral Flux (mW/nm)'], label=ln)
    lamp = lamp.interpolate(method='nearest').ffill().bfill().reindex(wavelength_grid, method='nearest')
    lamps[ln] = lamp
    lamp.to_csv(f'sources/thorlabs/LEDs/{ln}.csv')
    print(lamp)

plt.show()
